---
title: Sobre o LaKademy
slug: sobre
---

O LaKademy ‒ KDE Latin America Summit ‒ é um encontro de usuários e colaboradores latino-americanos da comunidade [KDE](https://www.kde.org/), uma das maiores comunidades de software livre do mundo.

O evento é realizado desde 2012 e foi um desdobramento do [Akademy-Br](https://wiki.br.kde.org/Akademy-BR_2010), única edição do encontro de colaboradores brasileiros do KDE, realizado em 2010. Após o Akademy-Br tornou-se evidente a importância de um encontro que integrasse não apenas brasileiros, mas colaboradores de toda a América Latina.

O principal objetivo do LaKademy, portanto, é promover encontros presenciais da comunidade latino-americana do KDE, possibilitando um momento de imersão nos projetos da comunidade. O LaKademy pretende funcionar como uma espaço de:

*   Troca de ideias sobre projetos e iniciativas entre os diferentes membros da comunidade;
*   Sessões de _hacking_, nas quais os colaboradores colocam a mão na massa;
*   Planejamento de futuras ações para o KDE na América Latina e no mundo;
*   Apresentação do projeto para novos e potenciais colaboradores.
