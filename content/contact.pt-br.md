---
title: Contato
slug: contato
---

O LaKademy é organizado pela [Equipe do KDE Brasil](https://br.kde.org) com o apoio da entidade não-financeira [KDE e.V](https://ev.kde.org).

Temos vários grupos assim como outros tipos de contato. Alguns dos grupos estão conectados entre si, permitindo mensagens entre plataformas.

## Telegram

Os seguintes grupos podem ser acessados tendo uma conta no [Telegram](https://telegram.org/).

* Grupo geral da KDE: https://t.me/kdebrasil

* Desenvolvimento: https://t.me/KdeBrasilDesenvolvimento

* Tradução: https://t.me/kdel10nptbr

* Documentação: https://t.me/+o-R5X_nV92U5OGZh

* Offtopic: https://t.me/komunidade

Também há um grupo dedicado aos falantes de espanhol:

* Geral: https://t.me/kde_canasbravas

## Matrix

Os seguintes grupos podem ser acessados tendo uma conta no [Matrix](https://matrix.org/) e usando um [cliente oficial](https://matrix.org/clients/), como o [Element](https://matrix.org/docs/projects/client/element) ou o [Neochat](https://apps.kde.org).

* Grupo geral da KDE: https://go.kde.org/matrix/#/#kde-brasil:kde.org

* Tradução: https://go.kde.org/matrix/#/#kdebrasil-traducao:kde.org

* Documentação: https://go.kde.org/matrix/#/#kde-brasil-docs:kde.org

Também há grupos dedicados aos falantes de espanhol:

* América Latina: https://go.kde.org/matrix/#/#freenode_#kde-latam:matrix.org

* Argentina: https://go.kde.org/matrix/#/#freenode_#kde-ar:matrix.org

* Falantes de espanhol: https://go.kde.org/matrix/#/#freenode_#kde-es:matrix.org

## Outros

### IRC

Os seguintes grupos podem ser acessados usando um cliente de [IRC](https://community.kde.org/Internet_Relay_Chat).

* Geral, Brasil: irc://irc.libera.chat/kde-brasil

* América Latina: irc://irc.libera.chat/kde-pt

* Falantes de espanhol: irc://irc.libera.chat/kdehispano

### Lista de email

A lista de email brasileira pode ser usada para contatar a equipe do KDE Brasil.

* kde-br: https://mail.kde.org/mailman/listinfo/kde-br
