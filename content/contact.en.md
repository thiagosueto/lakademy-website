---
title: Contact
slug: contact
---

LaKademy is organized by the [KDE Brasil team](https://br.kde.org) with support from the non-profit organization [KDE e.V](https://ev.kde.org).

We have several groups available as well as other forms of contact. Some of the groups are interconnected, allowing you to message people from other platforms.

## Telegram

The following groups can be accessed with a [Telegram](https://telegram.org/) account.

* General Brazilian KDE group: https://t.me/kdebrasil

* Development: https://t.me/KdeBrasilDesenvolvimento

* Translation: https://t.me/kdel10nptbr

* Documentation: https://t.me/+o-R5X_nV92U5OGZh

* Offtopic: https://t.me/komunidade

Também há um grupo dedicado aos falantes de espanhol:

* General group: https://t.me/kde_canasbravas

## Matrix

The following groups can be accessed with a [Matrix](https://matrix.org/) account by using an [official client](https://matrix.org/clients/) like [Element](https://matrix.org/docs/projects/client/element) or [Neochat](https://apps.kde.org).

* General Brazilian KDE group: https://go.kde.org/matrix/#/#kde-brasil:kde.org

* Translation: https://go.kde.org/matrix/#/#kdebrasil-traducao:kde.org

* Documentation: https://go.kde.org/matrix/#/#kde-brasil-docs:kde.org

We also have groups that are dedicated to Spanish speakers:

* Latin America: https://go.kde.org/matrix/#/#freenode_#kde-latam:matrix.org

* Argentina: https://go.kde.org/matrix/#/#freenode_#kde-ar:matrix.org

* Spanish speakers: https://go.kde.org/matrix/#/#freenode_#kde-es:matrix.org

## Other

### IRC

The following groups can be accessed using an [IRC](https://community.kde.org/Internet_Relay_Chat) client.

* General, Brasil: irc://irc.libera.chat/kde-brasil

* Latin America: irc://irc.libera.chat/kde-pt

* Spanish speakers: irc://irc.libera.chat/kdehispano

### Mailing list

The Brazilian mailing list may be used to contact the KDE Brasil team.

* kde-br: https://mail.kde.org/mailman/listinfo/kde-br
