---
title: Contacto
slug: contacto
---

LaKademy está organizada por el [equipo de KDE Brasil](https://br.kde.org) con el apoyo de la entidad no financiera [KDE e.V](https://ev.kde.org).

Tenemos varios grupos, así como otros métodos de contacto. Algunos de los grupos están conectados entre sí, permitiendo la mensajería entre plataformas.

## Telegram

Se puede acceder a los siguientes grupos teniendo una cuenta en [Telegram](https://telegram.org/).

* Grupo KDE general: https://t.me/kdebrasil

* Desarrollo: https://t.me/KdeBrasilDesenvolvimento

* Traducción: https://t.me/kdel10nptbr

* Documentación: https://t.me/+o-R5X_nV92U5OGZh

* Offtopic: https://t.me/komunidade

También hay un grupo dedicado a los hispanohablantes:

* Hispano: https://t.me/kde_canasbravas

## Matrix

Se puede acceder a los siguientes grupos teniendo una cuenta en [Matrix](https://matrix.org/) y utilizando un [cliente oficial](https://matrix.org/clients/), como [Element](https://matrix.org/docs/projects/client/element) o [Neochat](https://apps.kde.org).

* Grupo General KDE: https://go.kde.org/matrix/#/#kde-brasil:kde.org

* Traducción: https://go.kde.org/matrix/#/#kdebrasil-traducao:kde.org

* Documentación: https://go.kde.org/matrix/#/#kde-brasil-docs:kde.org

También hay grupos dedicados a hispanohablantes:

* América Latina: https://go.kde.org/matrix/#/#freenode_#kde-latam:matrix.org

* Argentina: https://go.kde.org/matrix/#/#freenode_#kde-ar:matrix.org

* Hispanohablantes: https://go.kde.org/matrix/#/#freenode_#kde-es:matrix.org

## Otros

### IRC

Se puede acceder a los siguientes grupos utilizando un cliente [IRC] (https://community.kde.org/Internet_Relay_Chat).

* General, Brasil: irc://irc.libera.chat/kde-brasil

* América Latina: irc://irc.libera.chat/kde-pt

* Hispanohablantes: irc://irc.libera.chat/kdehispano

### Lista de correo

La lista de correo brasileña puede utilizarse para contactar con el equipo de KDE Brasil.

* kde-br: https://mail.kde.org/mailman/listinfo/kde-br
