---
title: About LaKademy
slug: about
---

LaKademy - the KDE Latin America Summit - is a meeting for the Latin American community of [KDE](https://www.kde.org/) users and contributors.

This event exists since 2012 and is a result of [Akademy-Br](https://wiki.br.kde.org/Akademy-BR_2010), the meeting between KDE Brazilian contributors meeting that was done only once, in 2010. After Akademy-Br, it was clear that we should have a meeting that integrates not only Brazilians, but other Latin American contributors.

The main goal of LaKademy is to promote personal meetings for the KDE Latin American community, allowing for moments of immersion in community projects. LaKademy aims to be a space for:

*   Exchanging ideas about projects and initiatives between different members of the community;
*   Hacking sessions in which contributors work in person;
*   Planning future actions for KDE in Latin America and the world;
*   Presenting the project to new and potential contributors.
