---
title: Sobre LaKademy
slug: sobre
---

LaKademy - KDE Latin America Summit - es un encuentro de usuarios y colaboradores latinoamericanos de la comunidad [KDE](https://www.kde.org/), una de las mayores comunidades de software libre del mundo.

El evento se realiza desde 2012 y fue una derivación de [Akademy-Br](https://wiki.br.kde.org/Akademy-BR_2010), un encuentro de contribuidores brasileños de KDE que solo duró una edición, realizada en 2010. Después de Akademy-Br se hizo evidente la importancia de una reunión que integró no sólo los brasileños, pero los contribuyentes de toda América Latina.

El objetivo principal de LaKademy es promover encuentros presenciales de la comunidad latinoamericana de KDE, permitiendo un momento de inmersión en los proyectos de la comunidad. LaKademy pretende funcionar como un espacio para:

* Intercambio de ideas sobre proyectos e iniciativas entre diferentes miembros de la comunidad;
* Sesiones de hacking, en las que los colaboradores se ensucian las manos;
* Planificación de acciones futuras para KDE en América Latina y el mundo;
* Presentación del proyecto a nuevos y potenciales colaboradores.
