# Template Website in Hugo

Use `hugo server` to run the website locally in http://localhost:1313/.

If you want to run this on your network, specify `--bind` for your local ip, `--port` for the desired port and `--baseURL` with an IP or http/https.

`hugo server --baseURL 10.10.10.1 --bind 10.10.10.1 --port 3000`

`hugo server --baseURL http://myserver.example --bind 10.10.10.1 --port 4040`

### Creating content

For the content, we can use Markdown + [Hugo shortcodes](https://gohugo.io/content-management/shortcodes/). The kde-hugo project also provides [its own shortcodes](https://invent.kde.org/websites/hugo-kde/-/tree/master/layouts/shortcodes), and custom ones can also be made.

For designing the website, we can use [Hugo templates](https://gohugo.io/templates/introduction/), [Bootstrap](https://getbootstrap.com/docs/5.3/getting-started/introduction/), and the [available styling provided by kde-hugo](https://invent.kde.org/websites/hugo-kde/-/tree/master/assets/scss).

### Learning resources

To get started with Hugo, I recommend [this beginner tutorial](https://cloudcannon.com/tutorials/hugo-beginner-tutorial/) and [this video](https://www.youtube.com/watch?v=Vj5zy2q7O9U).

To get started with Bootstrap, the [w3schools course](https://www.w3schools.com/bootstrap5/) is a gentle introduction.

### Website Structure

The entrypoint is "config.toml". TOML was used to avoid indentation hell, there are many settings to manage.

We use [translation by filename](https://gohugo.io/content-management/multilingual/#translation-by-filename) since it makes content and URL management simpler.

The translation configuration file is in "config/_default/languages.toml". We use `pt-br` (Brazilian Portuguese), `en` (English), `es` (Spanish), in that order. The website defaults to `pt-br`, so its language code is never shown.

Everything in the `[params]` section of "config.toml" and `[<languagecode>.params]` can be accessed with `.Site.Params.`, e.g. `{{ .Site.Params.description }}`.

Each language contains a `permalinks` section (e.g. `editions = ediciones`) that matches each section name (currently only `editions`) to the section url (in "content/editions/_index.\<languagecode\>.md") followed by the filename, this way "content/editions/2012.es.md" becomes `https://lakademy.kde.org/ediciones/2012/`.

Each language has menu entries for the navigation bar. We use `pageRef` to avoid [this bug](https://github.com/gohugoio/hugo/issues/9150). Never use `url` for navbars. `pageRef` was introduced in 0.86, so make sure your hugo version is up to date.

If you create an individual page (directly in "content/"), make sure to use `slug` to set a translated filename.

The strings in "i18n/\<languagecode\>.yaml" are used in "layouts/index.html". If we have a key `home.readmore` that is present in all three files, using `{{ i18n "home.readmore" }}` will fetch the correct translation based on the displayed language. This is to be used only in layout files, and it allows for multilingual content in the main page.

In other words, we translate three parts of the website: individual pages (via translation by filename, e.g. "2012.en.md", "2012.es.md", "2012.pt-br.md"), the navigation bar (in "config/_default/languages.toml"), and the homepage strings (in "i18n/en.yaml", "i18n/es.yaml", "i18n/pt-br.yaml").

### Development
Read about the shared theme at [kde-hugo](https://invent.kde.org/websites/hugo-kde)

### I18n

We don't use hugo-i18n, which is used to outsource the translation files to other KDE translators.

If we ever do want that, see the [readme in hugo-i18n](https://invent.kde.org/websites/hugo-i18n).
